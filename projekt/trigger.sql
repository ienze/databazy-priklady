/*
 * TRIGGERS
 */

CREATE OR REPLACE TRIGGER user_written_articles_trigger AFTER INSERT
    ON articles 
FOR EACH ROW
BEGIN
    UPDATE users SET written_articles=written_articles+1 WHERE id=:NEW.create_user;
END;
/

CREATE OR REPLACE TRIGGER articles_audit_trigger AFTER INSERT OR DELETE OR UPDATE
    ON articles 
FOR EACH ROW
DECLARE
    action VARCHAR(50);
BEGIN
    IF INSERTING THEN
        action := 'inserted';
    ELSIF UPDATING THEN
        action := 'updated';
    ELSIF DELETING THEN
        action := 'deleted';
    END IF;

    INSERT INTO articles_audit(id, article_id, title, content, create_user, edit_user, published, change_date, action)
        VALUES (articles_audit_sequence.NEXTVAL, :NEW.id, :NEW.title, :NEW.content, :NEW.create_user, :NEW.edit_user, :NEW.published, CURRENT_DATE, action);
END;
/