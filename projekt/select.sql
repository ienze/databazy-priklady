/*
 * SELECTS
 */

/* Edit log */
CREATE OR REPLACE VIEW articles_audit_log(log, article_id, user_id) AS 
SELECT E.name || ' ' || A.action || ' ' || C.name || '''s article ' || A.title, A.article_id, A.edit_user
FROM articles_audit A, users C, users E WHERE A.create_user = C.id AND A.edit_user = E.id;

SELECT log FROM articles_audit_log WHERE article_id = 1;
SELECT log FROM articles_audit_log WHERE user_id = 1;
SELECT log FROM articles_audit_log WHERE user_id = 2;

/* All no longer used titles of articles */
SELECT title FROM articles_audit MINUS SELECT title FROM articles;

/* Select contribution for each edit */
CREATE TYPE contribution_entry IS OBJECT(
    user_id number,
    contribution REAL
);
/

CREATE TYPE contribution_table IS TABLE OF contribution_entry;
/

CREATE OR REPLACE FUNCTION contribution(search_article_id number) RETURN contribution_table IS
    old_content VARCHAR2(2000) := '•)';
    contribution_t contribution_table := contribution_table();
    CURSOR articles_audit_cursor IS SELECT content, edit_user FROM articles_audit WHERE article_id=search_article_id ORDER BY change_date ASC;
    row articles_audit_cursor%ROWTYPE;
    n integer := 0;
BEGIN
    FOR row IN articles_audit_cursor LOOP
        
        contribution_t.extend;
        n := n + 1;
        contribution_t(n) := contribution_entry(
            row.edit_user,
            (LENGTH(row.content) - LENGTH(old_content))
        );

        old_content := row.content;
    END LOOP;

    RETURN contribution_t;
END contribution;
/

/* Select contribution per user */
SELECT SUM(contribution) AS contribution, user_id, (SELECT name FROM users WHERE id = user_id) AS name
    FROM TABLE (contribution(1)) GROUP BY user_id;

SELECT COUNT(*) AS count, create_user AS user_id FROM articles GROUP BY create_user HAVING COUNT(*) >= 3;