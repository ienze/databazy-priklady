/*
 * Projekt na PV003 - Architektura relačních databázových systémů
 * Dominik Gmiterko
 */

/*
 * STRUCTURE
 */

CREATE TABLE blogs(
    id NUMBER CONSTRAINT blog_pk PRIMARY KEY,
    title VARCHAR2(100) CONSTRAINT blog_title_unique UNIQUE
);

CREATE SEQUENCE blogs_sequence;

CREATE TABLE users(
    id NUMBER CONSTRAINT user_pk PRIMARY KEY,
    name VARCHAR2(100) CONSTRAINT user_name_unique UNIQUE,
    written_articles NUMBER
);

CREATE SEQUENCE users_sequence;

CREATE TABLE articles(
    id NUMBER CONSTRAINT article_pk PRIMARY KEY,
    blog NUMBER,
    title VARCHAR2(100),
    content VARCHAR2(2000),
    create_user NUMBER,
    edit_user NUMBER,
    published DATE,
    CONSTRAINT fk_articles_blog
        FOREIGN KEY (blog)
        REFERENCES blogs(id),
    CONSTRAINT fk_articles_create_user
        FOREIGN KEY (create_user)
        REFERENCES users(id),
    CONSTRAINT fk_articles_edit_user
        FOREIGN KEY (edit_user)
        REFERENCES users(id)
);

CREATE SEQUENCE articles_sequence;

CREATE TABLE articles_audit(
    id NUMBER CONSTRAINT articles_audit_pk PRIMARY KEY,
    article_id NUMBER,
    title VARCHAR2(100),
    content VARCHAR2(2000),
    create_user NUMBER,
    edit_user NUMBER,
    published DATE,
    change_date DATE,
    action VARCHAR(50),
    CONSTRAINT fk_articles_audit_create_user
        FOREIGN KEY (create_user)
        REFERENCES users(id),
    CONSTRAINT fk_articles_audit_edit_user
        FOREIGN KEY (edit_user)
        REFERENCES users(id)
);

CREATE SEQUENCE articles_audit_sequence;

/*
 * TRIGGERS
 */

CREATE OR REPLACE TRIGGER user_written_articles_trigger AFTER INSERT
    ON articles 
FOR EACH ROW
BEGIN
    UPDATE users SET written_articles=written_articles+1 WHERE id=:NEW.create_user;
END;
/

CREATE OR REPLACE TRIGGER articles_audit_trigger AFTER INSERT OR DELETE OR UPDATE
    ON articles 
FOR EACH ROW
DECLARE
    action VARCHAR(50);
BEGIN
    IF INSERTING THEN
        action := 'inserted';
    ELSIF UPDATING THEN
        action := 'updated';
    ELSIF DELETING THEN
        action := 'deleted';
    END IF;

    INSERT INTO articles_audit(id, article_id, title, content, create_user, edit_user, published, change_date, action)
        VALUES (articles_audit_sequence.NEXTVAL, :NEW.id, :NEW.title, :NEW.content, :NEW.create_user, :NEW.edit_user, :NEW.published, CURRENT_DATE, action);
END;
/

/*
 * DATA
 */

INSERT INTO blogs(id, title) VALUES(blogs_sequence.NEXTVAL, 'Fleeting Thoughts');
INSERT INTO blogs(id, title) VALUES(blogs_sequence.NEXTVAL, 'Recepty pro vařiče čajů');

INSERT INTO users(id, name, written_articles) VALUES(users_sequence.NEXTVAL, 'Dominik', 0);
INSERT INTO users(id, name, written_articles) VALUES(users_sequence.NEXTVAL, 'Linda', 0);

INSERT INTO articles(id, blog, title, content, create_user, edit_user, published) VALUES(articles_sequence.NEXTVAL, 1, 'The beginning of the end', 'Dark secrets are revealed as Coulson and his team put everything on the line to stop Garrett and the forces of HYDRA, on the explosive season finale.', 1, 1, CURRENT_DATE);
INSERT INTO articles(id, blog, title, content, create_user, edit_user, published) VALUES(articles_sequence.NEXTVAL, 2, 'Palačinky', 'Recept na Banánové minipalačinky tu už byl. Ony ty klasické nejsou o moc těžší, jen je na ně potřeba více surovin.', 2, 2, CURRENT_DATE);
INSERT INTO articles(id, blog, title, content, create_user, edit_user, published) VALUES(articles_sequence.NEXTVAL, 1, 'Cuketa v arabském chlebu', 'Veganská videa, kde vždycky jen něco nakrájej, hoděj na pánev a zalejou protlakem, mě inspirovala k dnešní večeři.', 2, 2, CURRENT_DATE);

UPDATE articles SET title='Beginning of the end', edit_user = 2 WHERE id = 1;

/*
 * SELECTS
 */

/* Edit log */
CREATE OR REPLACE VIEW articles_audit_log(log, article_id, user_id) AS 
SELECT E.name || ' ' || A.action || ' ' || C.name || '''s article ' || A.title, A.article_id, A.edit_user
FROM articles_audit A, users C, users E WHERE A.create_user = C.id AND A.edit_user = E.id;

SELECT log FROM articles_audit_log;
SELECT log FROM articles_audit_log WHERE article_id = 1;
SELECT log FROM articles_audit_log WHERE user_id = 2;
/* Example:
Linda inserted Linda's article The beginning of the end
Linda inserted Linda's article Cuketa v arabském chlebu
Linda updated Dominik's article Beginning of the end
*/

/* All no longer used titles of articles */
SELECT title FROM articles_audit MINUS SELECT title FROM articles;

/* Select users with more than 3 articles */
SELECT (SELECT name FROM users WHERE id = create_user) AS name FROM articles GROUP BY create_user HAVING COUNT(*) >= 3;

/*
 * FUNCTION
 */

/* Select contribution for each edit */
CREATE TYPE contribution_entry IS OBJECT(
    user_id number,
    contribution REAL
);
/

CREATE TYPE contribution_table IS TABLE OF contribution_entry;
/

CREATE OR REPLACE FUNCTION contribution(search_article_id number) RETURN contribution_table IS
    old_content VARCHAR2(2000) := '•)';
    contribution_t contribution_table := contribution_table();
    CURSOR articles_audit_cursor IS SELECT content, edit_user FROM articles_audit WHERE article_id=search_article_id ORDER BY change_date ASC;
    row articles_audit_cursor%ROWTYPE;
    n integer := 0;
BEGIN
    FOR row IN articles_audit_cursor LOOP
        
        contribution_t.extend;
        n := n + 1;
        contribution_t(n) := contribution_entry(
            row.edit_user,
            (LENGTH(row.content) - LENGTH(old_content))
        );

        old_content := row.content;
    END LOOP;

    RETURN contribution_t;
END contribution;
/

/* Select contribution per user */
SELECT (SELECT name FROM users WHERE id = user_id) AS name, SUM(contribution) AS contribution, user_id
    FROM TABLE (contribution(1)) GROUP BY user_id ORDER BY contribution DESC;
/*Example:
NAME     CONTRIBUTION    USER_ID
-------- ------------ ----------
Linda             145          2
Dominik             0          1
*/