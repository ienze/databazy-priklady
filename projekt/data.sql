/*
 * DATA
 */

INSERT INTO blogs(id, title) VALUES(blogs_sequence.NEXTVAL, 'Fleeting Thoughts');
INSERT INTO blogs(id, title) VALUES(blogs_sequence.NEXTVAL, 'Recepty pro vařiče čajů');

INSERT INTO users(id, name, written_articles) VALUES(users_sequence.NEXTVAL, 'Dominik', 0);
INSERT INTO users(id, name, written_articles) VALUES(users_sequence.NEXTVAL, 'Linda', 0);

INSERT INTO articles(id, blog, title, content, create_user, edit_user, published) VALUES(articles_sequence.NEXTVAL, 1, 'The beginning of the end', 'Dark secrets are revealed as Coulson and his team put everything on the line to stop Garrett and the forces of HYDRA, on the explosive season finale.', 1, 1, CURRENT_DATE);
INSERT INTO articles(id, blog, title, content, create_user, edit_user, published) VALUES(articles_sequence.NEXTVAL, 2, 'Palačinky', 'Recept na Banánové minipalačinky tu už byl. Ony ty klasické nejsou o moc těžší, jen je na ně potřeba více surovin.', 2, 2, CURRENT_DATE);
INSERT INTO articles(id, blog, title, content, create_user, edit_user, published) VALUES(articles_sequence.NEXTVAL, 1, 'Cuketa v arabském chlebu', 'Veganská videa, kde vždycky jen něco nakrájej, hoděj na pánev a zalejou protlakem, mě inspirovala k dnešní večeři.', 2, 2, CURRENT_DATE);

UPDATE articles SET title='Beginning of the end', edit_user=2 WHERE id = 1;