/*
 * STRUCTURE
 */

CREATE TABLE blogs(
    id NUMBER CONSTRAINT blog_pk PRIMARY KEY,
    title VARCHAR2(100) CONSTRAINT blog_title_unique UNIQUE
);

CREATE SEQUENCE blogs_sequence;

CREATE TABLE users(
    id NUMBER CONSTRAINT user_pk PRIMARY KEY,
    name VARCHAR2(100) CONSTRAINT user_name_unique UNIQUE,
    written_articles NUMBER
);

CREATE SEQUENCE users_sequence;

CREATE TABLE articles(
    id NUMBER CONSTRAINT article_pk PRIMARY KEY,
    blog NUMBER,
    title VARCHAR2(100),
    content VARCHAR2(2000),
    create_user NUMBER,
    edit_user NUMBER,
    published DATE,
    CONSTRAINT fk_articles_blog
        FOREIGN KEY (blog)
        REFERENCES blogs(id),
    CONSTRAINT fk_articles_create_user
        FOREIGN KEY (create_user)
        REFERENCES users(id),
    CONSTRAINT fk_articles_edit_user
        FOREIGN KEY (edit_user)
        REFERENCES users(id)
);

CREATE SEQUENCE articles_sequence;

CREATE TABLE articles_audit(
    id NUMBER CONSTRAINT articles_audit_pk PRIMARY KEY,
    article_id NUMBER,
    title VARCHAR2(100),
    content VARCHAR2(2000),
    create_user NUMBER,
    edit_user NUMBER,
    published DATE,
    change_date DATE,
    action VARCHAR(50),
    CONSTRAINT fk_articles_audit_create_user
        FOREIGN KEY (create_user)
        REFERENCES users(id),
    CONSTRAINT fk_articles_audit_edit_user
        FOREIGN KEY (edit_user)
        REFERENCES users(id)
);

CREATE SEQUENCE articles_audit_sequence;