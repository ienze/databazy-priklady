/*
 * DROP
 */
DROP SEQUENCE blogs_sequence;
DROP SEQUENCE users_sequence;
DROP SEQUENCE articles_sequence;
DROP SEQUENCE articles_audit_sequence;

DROP TABLE articles_audit;
DROP TABLE articles;
DROP TABLE blogs;
DROP TABLE users;

DROP TRIGGER user_written_articles_trigger;
DROP TRIGGER articles_audit_trigger;

DROP VIEW articles_audit_log;

DROP TYPE contribution_entry;
DROP TYPE contribution_table;
DROP FUNCTION contribution;