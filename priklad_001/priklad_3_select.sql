-- Kedze datova struktura v ktorej su ulozene data je zlozita rozhodol som sa vytvorit proceduru na zobrazenie 
-- planu v danom casovom rozsahu. V plane sa zobrazia aj vynimocne pripady (dovolenka, mimoriadne obsluzenie).

DELIMITER //
CREATE DEFINER=`root`@`localhost` PROCEDURE `view_plan`(IN `from_date` DATE, IN `to_date` DATE)
    READS SQL DATA
    DETERMINISTIC
    SQL SECURITY INVOKER
BEGIN
DECLARE d DATE DEFAULT from_date;
DECLARE plan INT;

CREATE TEMPORARY TABLE IF NOT EXISTS result (
	`day` DATE NOT NULL,
	`plan` INT,
   `car` INT NOT NULL,
   `customer` INT NOT NULL
);
TRUNCATE TABLE result;

for_dates: LOOP

	SELECT id FROM plans WHERE d BETWEEN `active_from` AND `active_to` LIMIT 1 INTO plan;

	INSERT INTO result SELECT d AS `day`, `plan`, `car`, `customer` FROM plan_items WHERE `plan` = plan AND `day_in_week` = WEEKDAY(d)
		AND NOT EXISTS (SELECT id FROM plan_changes WHERE `date` = d AND `change_type` = 0);
	
	INSERT INTO result SELECT d AS `day`, NULL AS `plan`, `car`, `customer` FROM plan_changes WHERE d = `date` AND `change_type` = 1;
	
	SET d = ADDDATE(d, INTERVAL 1 DAY) ;
	IF d < to_date THEN
		ITERATE for_dates;
	END IF;
	LEAVE for_dates;
END LOOP for_dates;

SELECT * FROM result;

END//
DELIMITER ;