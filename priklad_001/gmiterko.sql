-- --------------------------------
-- Author:         Dominik Gmiterko
-- Database:       MySQL - 5.6.21  
-- --------------------------------

--
-- ZAKLADACI SKRIPT
--

CREATE TABLE `cars` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
);

CREATE TABLE `customers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(64) NOT NULL,
  `address` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
);

-- Tabulka sluzi na vytvaranie verzii planou, kazdy plan ma zadany cas v akom je aktyvny.
CREATE TABLE `plans` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `active_from` date NOT NULL,
  `active_to` date NOT NULL,
  PRIMARY KEY (`id`)
);

-- Tabulka uklada vynymocne pripady (dovolenka, mimoriadne obsluzenie)
CREATE TABLE `plan_changes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `car` int(11) DEFAULT NULL,
  `customer` int(11) DEFAULT NULL,
  `date` int(11) DEFAULT NULL,
  `change_type` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `plan_changes_cars` (`car`),
  KEY `plan_changes_customers` (`customer`),
  CONSTRAINT `plan_changes_cars` FOREIGN KEY (`car`) REFERENCES `cars` (`id`),
  CONSTRAINT `plan_changes_customers` FOREIGN KEY (`customer`) REFERENCES `customers` (`id`)
);

-- Tabulka uklada plany v beznom tyzni
CREATE TABLE `plan_items` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `plan` int(11) NOT NULL,
  `car` int(11) NOT NULL,
  `customer` int(11) NOT NULL,
  `day_in_week` smallint(6) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `plan_item_plan` (`plan`),
  KEY `plan_item_cars` (`car`),
  KEY `plan_item_customer` (`customer`),
  CONSTRAINT `plan_item_cars` FOREIGN KEY (`car`) REFERENCES `cars` (`id`),
  CONSTRAINT `plan_item_customer` FOREIGN KEY (`customer`) REFERENCES `customers` (`id`),
  CONSTRAINT `plan_item_plan` FOREIGN KEY (`plan`) REFERENCES `plans` (`id`)
);

--
-- TESTOVACIE DATA
--

INSERT INTO `cars` (`id`, `name`) VALUES
  (1, 'A'),
  (2, 'B');

INSERT INTO `customers` (`id`, `name`, `address`) VALUES
  (1, 'Mollie Buckley', '572-3965 Luctus Av.'),
  (2, 'Briar Herrera', 'P.O. Box 786, 2257 Tempor Avenue'),
  (3, 'Haley Thornton', '251-7064 Dictum Av.'),
  (4, 'Yeo Stark', '5293 Mollis. St.'),
  (5, 'Harriet Thornton', 'P.O. Box 132, 1893 Nec Road'),
  (6, 'Marcia Davidson', 'P.O. Box 568, 8596 Aenean Avenue'),
  (7, 'Mollie Cain', 'Ap #912-3228 Aliquet. Avenue'),
  (8, 'Linda Potter', 'Ap #262-1159 Turpis Ave'),
  (9, 'Ivy Harrell', '224-1220 Nec, Street'),
  (10, 'Mallory Higgins', 'Ap #625-409 Tellus. Avenue');

INSERT INTO `plans` (`id`, `active_from`, `active_to`) VALUES
  (1, '2016-05-06', '2016-12-31');

INSERT INTO `plan_changes` (`id`, `car`, `customer`, `date`, `change_type`) VALUES
  (1, 2, 8, '2016-05-04', 0),
  (2, 2, 8, '2016-05-05', 1);

INSERT INTO `plan_items` (`id`, `plan`, `car`, `customer`, `day_in_week`) VALUES
  (1, 1, 2, 1, 0),
  (2, 1, 1, 2, 0),
  (3, 1, 2, 8, 2),
  (4, 1, 1, 9, 4);

--
-- SELECT PLANU
--

-- Kedze datova struktura v ktorej su ulozene data je zlozita rozhodol som sa vytvorit proceduru na zobrazenie 
-- planu v danom casovom rozsahu. V plane sa zobrazia aj vynimocne pripady (dovolenka, mimoriadne obsluzenie).

DELIMITER //
CREATE DEFINER=`root`@`localhost` PROCEDURE `view_plan`(IN `from_date` DATE, IN `to_date` DATE)
    READS SQL DATA
    DETERMINISTIC
    SQL SECURITY INVOKER
BEGIN
DECLARE d DATE DEFAULT from_date;
DECLARE plan INT;

CREATE TEMPORARY TABLE IF NOT EXISTS result (
  `day` DATE NOT NULL,
  `plan` INT,
   `car` INT NOT NULL,
   `customer` INT NOT NULL
);
TRUNCATE TABLE result;

for_dates: LOOP

  SELECT id FROM plans WHERE d BETWEEN `active_from` AND `active_to` LIMIT 1 INTO plan;

  INSERT INTO result SELECT d AS `day`, `plan`, `car`, `customer` FROM plan_items WHERE `plan` = plan AND `day_in_week` = WEEKDAY(d)
    AND NOT EXISTS (SELECT id FROM plan_changes WHERE `date` = d AND `change_type` = 0);
  
  INSERT INTO result SELECT d AS `day`, NULL AS `plan`, `car`, `customer` FROM plan_changes WHERE d = `date` AND `change_type` = 1;
  
  SET d = ADDDATE(d, INTERVAL 1 DAY) ;
  IF d < to_date THEN
    ITERATE for_dates;
  END IF;
  LEAVE for_dates;
END LOOP for_dates;

SELECT * FROM result;

END//
DELIMITER ;