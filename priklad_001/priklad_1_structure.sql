-- --------------------------------
-- Author:         Dominik Gmiterko
-- Database:       MySQL - 5.6.21  
-- --------------------------------

CREATE TABLE `cars` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
);

CREATE TABLE `customers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(64) NOT NULL,
  `address` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
);

-- Tabulka sluzi na vytvaranie verzii planou, kazdy plan ma zadany cas v akom je aktyvny.
CREATE TABLE `plans` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `active_from` date NOT NULL,
  `active_to` date NOT NULL,
  PRIMARY KEY (`id`)
);

-- Tabulka uklada vynymocne pripady (dovolenka, mimoriadne obsluzenie)
CREATE TABLE `plan_changes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `car` int(11) DEFAULT NULL,
  `customer` int(11) DEFAULT NULL,
  `date` int(11) DEFAULT NULL,
  `change_type` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `plan_changes_cars` (`car`),
  KEY `plan_changes_customers` (`customer`),
  CONSTRAINT `plan_changes_cars` FOREIGN KEY (`car`) REFERENCES `cars` (`id`),
  CONSTRAINT `plan_changes_customers` FOREIGN KEY (`customer`) REFERENCES `customers` (`id`)
);

-- Tabulka uklada plany v beznom tyzni
CREATE TABLE `plan_items` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `plan` int(11) NOT NULL,
  `car` int(11) NOT NULL,
  `customer` int(11) NOT NULL,
  `day_in_week` smallint(6) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `plan_item_plan` (`plan`),
  KEY `plan_item_cars` (`car`),
  KEY `plan_item_customer` (`customer`),
  CONSTRAINT `plan_item_cars` FOREIGN KEY (`car`) REFERENCES `cars` (`id`),
  CONSTRAINT `plan_item_customer` FOREIGN KEY (`customer`) REFERENCES `customers` (`id`),
  CONSTRAINT `plan_item_plan` FOREIGN KEY (`plan`) REFERENCES `plans` (`id`)
);
