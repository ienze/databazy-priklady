INSERT INTO `cars` (`id`, `name`) VALUES
	(1, 'A'),
	(2, 'B');

INSERT INTO `customers` (`id`, `name`, `address`) VALUES
	(1, 'Mollie Buckley', '572-3965 Luctus Av.'),
	(2, 'Briar Herrera', 'P.O. Box 786, 2257 Tempor Avenue'),
	(3, 'Haley Thornton', '251-7064 Dictum Av.'),
	(4, 'Yeo Stark', '5293 Mollis. St.'),
	(5, 'Harriet Thornton', 'P.O. Box 132, 1893 Nec Road'),
	(6, 'Marcia Davidson', 'P.O. Box 568, 8596 Aenean Avenue'),
	(7, 'Mollie Cain', 'Ap #912-3228 Aliquet. Avenue'),
	(8, 'Linda Potter', 'Ap #262-1159 Turpis Ave'),
	(9, 'Ivy Harrell', '224-1220 Nec, Street'),
	(10, 'Mallory Higgins', 'Ap #625-409 Tellus. Avenue');

INSERT INTO `plans` (`id`, `active_from`, `active_to`) VALUES
	(1, '2016-05-06', '2016-12-31');

INSERT INTO `plan_changes` (`id`, `car`, `customer`, `date`, `change_type`) VALUES
	(1, 2, 8, '2016-05-04', 0),
	(2, 2, 8, '2016-05-05', 1);

INSERT INTO `plan_items` (`id`, `plan`, `car`, `customer`, `day_in_week`) VALUES
	(1, 1, 2, 1, 0),
	(2, 1, 1, 2, 0),
	(3, 1, 2, 8, 2),
	(4, 1, 1, 9, 4);