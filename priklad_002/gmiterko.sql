-- --------------------------------
-- Author:         Dominik Gmiterko
-- Database:       MySQL - 5.6.21  
-- --------------------------------

--
-- ZAKLADACI SKRIPT
--

CREATE TABLE `cars` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) COLLATE='utf8_general_ci';

CREATE TABLE IF NOT EXISTS `customers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ico` int(11) DEFAULT '0',
  `name` varchar(64) NOT NULL,
  `town` varchar(64) NOT NULL,
  `address` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) COLLATE='utf8_general_ci';

-- Tabulka sluzi na vytvaranie verzii planou, kazdy plan ma zadany cas v akom je aktyvny.
CREATE TABLE `plans` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `active_from` date NOT NULL,
  `active_to` date NOT NULL,
  PRIMARY KEY (`id`)
) COLLATE='utf8_general_ci';

-- Tabulka uklada vynymocne pripady (dovolenka, mimoriadne obsluzenie)
CREATE TABLE `plan_changes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `car` int(11) DEFAULT NULL,
  `customer` int(11) DEFAULT NULL,
  `date` date DEFAULT NULL,
  `change_type` tinyint(4) DEFAULT NULL COMMENT '0 - remove item from plan, 1 - add item to plan',
  PRIMARY KEY (`id`),
  KEY `FK_plan_changes_cars` (`car`),
  KEY `FK_plan_changes_customers` (`customer`),
  KEY `date` (`date`),
  CONSTRAINT `FK_plan_changes_cars` FOREIGN KEY (`car`) REFERENCES `cars` (`id`),
  CONSTRAINT `FK_plan_changes_customers` FOREIGN KEY (`customer`) REFERENCES `customers` (`id`)
) COLLATE='utf8_general_ci';

-- Tabulka uklada plany v beznom tyzni
CREATE TABLE `plan_items` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `plan` int(11) NOT NULL,
  `car` int(11) NOT NULL,
  `customer` int(11) NOT NULL,
  `day_in_week` tinyint(4) NOT NULL COMMENT 'weekday 0-6',
  `week` tinyint(4) DEFAULT NULL COMMENT '0 - odd week, 1 - even week, null - every week',
  PRIMARY KEY (`id`),
  KEY `FK_plan_item_plan` (`plan`),
  KEY `FK_plan_item_cars` (`car`),
  KEY `FK_plan_item_customer` (`customer`),
  CONSTRAINT `FK_plan_item_cars` FOREIGN KEY (`car`) REFERENCES `cars` (`id`),
  CONSTRAINT `FK_plan_item_customer` FOREIGN KEY (`customer`) REFERENCES `customers` (`id`),
  CONSTRAINT `FK_plan_item_plan` FOREIGN KEY (`plan`) REFERENCES `plans` (`id`)
) COLLATE='utf8_general_ci';

--
-- SELECT PLANU
--

-- Kedze datova struktura v ktorej su ulozene data je zlozita rozhodol som sa vytvorit proceduru na zobrazenie 
-- planu v danom casovom rozsahu. V plane sa zobrazia aj vynimocne pripady (dovolenka, mimoriadne obsluzenie).

DELIMITER //
CREATE DEFINER=`root`@`localhost` PROCEDURE `view_plan`(IN `from_date` DATE, IN `to_date` DATE)
    READS SQL DATA
    DETERMINISTIC
    SQL SECURITY INVOKER
BEGIN
DECLARE d DATE DEFAULT from_date;
DECLARE plan INT;
DECLARE odd_week TINYINT DEFAULT 0;

CREATE TEMPORARY TABLE IF NOT EXISTS result (
	`day` DATE NOT NULL,
	`plan` INT,
   `car` INT NOT NULL,
   `customer` INT NOT NULL
);
TRUNCATE TABLE result;

for_dates: LOOP

	SELECT id FROM plans WHERE d BETWEEN `active_from` AND `active_to` LIMIT 1 INTO plan;
	
	INSERT INTO result SELECT d AS `day`, `plan`, `car`, `customer` FROM plan_items WHERE `plan` = plan AND `day_in_week` = WEEKDAY(d) AND (`week` IS NULL OR `week` = odd_week)
		AND NOT EXISTS (SELECT id FROM plan_changes WHERE `date` = d AND plan_changes.`car`= plan_items.`car` AND plan_changes.`customer`= plan_items.`customer` AND `change_type` = 0);
	
	INSERT INTO result SELECT d AS `day`, NULL AS `plan`, `car`, `customer` FROM plan_changes WHERE d = `date` AND `change_type` = 1;
	
	SET d = ADDDATE(d, INTERVAL 1 DAY) ;
	SET odd_week = WEEKOFYEAR(d) % 2;
	IF d < to_date THEN
		ITERATE for_dates;
	END IF;
	LEAVE for_dates;
END LOOP for_dates;

SELECT * FROM result;

END//
DELIMITER ;