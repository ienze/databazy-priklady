INSERT IGNORE INTO plans(`id`, `active_from`, `active_to`) VALUES(1, '2016-01-01', '2017-01-01');

-- Cars

INSERT INTO cars(name) VALUES('Auto 1');
SET @car_id_0 = LAST_INSERT_ID();
INSERT INTO cars(name) VALUES('Auto 2');
SET @car_id_1 = LAST_INSERT_ID();

-- Customers

INSERT INTO customers(ico, name, town, address) VALUES('123456001', 'Hotel A-Austerlitz', 'Brno-střed', 'Táborského nábřeží 3');
SET @customer_id_0 = LAST_INSERT_ID();
INSERT INTO customers(ico, name, town, address) VALUES('123456002', 'Hotel A-Podlesí', 'Brno-Kohoutovice', 'Žebětínská 1a');
SET @customer_id_1 = LAST_INSERT_ID();
INSERT INTO customers(ico, name, town, address) VALUES('123456003', 'Hotel Akord', 'Brno-Královo Pole', 'Palackého 118');
SET @customer_id_2 = LAST_INSERT_ID();
INSERT INTO customers(ico, name, town, address) VALUES('123456004', 'Hotel Albellus', 'Brno-Židenice', 'Rokycanova 21');
SET @customer_id_3 = LAST_INSERT_ID();
INSERT INTO customers(ico, name, town, address) VALUES('123456005', 'Hotel Amphone', 'Brno-střed', 'třída Kpt. Jaroše 27');
SET @customer_id_4 = LAST_INSERT_ID();
INSERT INTO customers(ico, name, town, address) VALUES('123456006', 'Hotel Arte', 'Brno-sever', 'Drobného 6');
SET @customer_id_5 = LAST_INSERT_ID();
INSERT INTO customers(ico, name, town, address) VALUES('123456007', 'Hotel Avanti', 'Brno-Královo Pole', 'Střední 61');
SET @customer_id_6 = LAST_INSERT_ID();
INSERT INTO customers(ico, name, town, address) VALUES('123456008', 'Hotel Avion', 'Brno-střed', 'Česká 20');
SET @customer_id_7 = LAST_INSERT_ID();
INSERT INTO customers(ico, name, town, address) VALUES('123456009', 'Hotel Barceló Brno Palace', 'Brno-střed', 'Šilingrovo náměstí 2');
SET @customer_id_8 = LAST_INSERT_ID();
INSERT INTO customers(ico, name, town, address) VALUES('123456010', 'Hotel Bílá růže', 'Brno-jih', 'Svatopetrská 26');
SET @customer_id_9 = LAST_INSERT_ID();
INSERT INTO customers(ico, name, town, address) VALUES('123456011', 'Bobycentrum Hotel', 'Brno-Královo Pole', 'Sportovní 2a');
SET @customer_id_10 = LAST_INSERT_ID();
INSERT INTO customers(ico, name, town, address) VALUES('123456012', 'Hotel Brno', 'Brno-střed', 'Horní 19');
SET @customer_id_11 = LAST_INSERT_ID();
INSERT INTO customers(ico, name, town, address) VALUES('123456013', 'City Apart Hotel', 'Brno-jih', 'Komárovské nábřeží 2');
SET @customer_id_12 = LAST_INSERT_ID();
INSERT INTO customers(ico, name, town, address) VALUES('123456014', 'Hotel Continental', 'Brno-střed', 'Kounicova 6');
SET @customer_id_13 = LAST_INSERT_ID();
INSERT INTO customers(ico, name, town, address) VALUES('123456015', 'Hotel Cyro Superior', 'Brno-střed', 'Anenská 9');
SET @customer_id_14 = LAST_INSERT_ID();
INSERT INTO customers(ico, name, town, address) VALUES('123456016', 'Hotel Global', 'Brno-Kohoutovice', 'Libušino údolí 58');
SET @customer_id_15 = LAST_INSERT_ID();
INSERT INTO customers(ico, name, town, address) VALUES('123456017', 'Grand Hotel Brno', 'Brno-střed', 'Benešova 18/20');
SET @customer_id_16 = LAST_INSERT_ID();
INSERT INTO customers(ico, name, town, address) VALUES('123456018', 'Hotel Holiday Inn Brno', 'Brno-střed', 'Křížkovského 20');
SET @customer_id_17 = LAST_INSERT_ID();
INSERT INTO customers(ico, name, town, address) VALUES('123456019', 'Hotel International', 'Brno-střed', 'Husova 16');
SET @customer_id_18 = LAST_INSERT_ID();
INSERT INTO customers(ico, name, town, address) VALUES('123456020', 'Hotel Jelenice', 'Brno-Bystrc', 'Rakovecká 71');
SET @customer_id_19 = LAST_INSERT_ID();
INSERT INTO customers(ico, name, town, address) VALUES('123456021', 'Hotel Jonathan', 'Brno-Kohoutovice', 'Potocká 42');
SET @customer_id_20 = LAST_INSERT_ID();
INSERT INTO customers(ico, name, town, address) VALUES('123456022', 'Hotel Kozák', 'Brno-Žabovřesky', 'Horova 30');
SET @customer_id_21 = LAST_INSERT_ID();
INSERT INTO customers(ico, name, town, address) VALUES('123456023', 'Hotel Kozí Horka', 'Brno-Bystrc', 'Rakovecká 15');
SET @customer_id_22 = LAST_INSERT_ID();
INSERT INTO customers(ico, name, town, address) VALUES('123456024', 'Hotel Maximus Resort', 'Brno-Kníničky', 'Kníničky 316');
SET @customer_id_23 = LAST_INSERT_ID();
INSERT INTO customers(ico, name, town, address) VALUES('123456025', 'Hotel Morávka', 'Brno-střed', 'Heršpická 7');
SET @customer_id_24 = LAST_INSERT_ID();
INSERT INTO customers(ico, name, town, address) VALUES('123456026', 'Hotel Myslivna', 'Brno-Kohoutovice', 'Nad Pisárkami 1');
SET @customer_id_25 = LAST_INSERT_ID();
INSERT INTO customers(ico, name, town, address) VALUES('123456027', 'Hotel Neptun', 'Brno-Bystrc', 'Přehradní');
SET @customer_id_26 = LAST_INSERT_ID();
INSERT INTO customers(ico, name, town, address) VALUES('123456028', 'Noem Arch Design Hotel', 'Brno-Královo Pole', 'Cimburkova 9');
SET @customer_id_27 = LAST_INSERT_ID();
INSERT INTO customers(ico, name, town, address) VALUES('123456029', 'Hotel Omega', 'Brno-střed', 'Křídlovická 19b');
SET @customer_id_28 = LAST_INSERT_ID();
INSERT INTO customers(ico, name, town, address) VALUES('123456030', 'Hotel Palacký', 'Brno-Královo Pole', 'Kolejní 2');
SET @customer_id_29 = LAST_INSERT_ID();
INSERT INTO customers(ico, name, town, address) VALUES('123456031', 'Parkhotel Brno', 'Brno-Jundrov', 'Veslařská 250');
SET @customer_id_30 = LAST_INSERT_ID();
INSERT INTO customers(ico, name, town, address) VALUES('123456032', 'Hotel Pegas Brno', 'Brno-střed', 'Jakubská 4');
SET @customer_id_31 = LAST_INSERT_ID();
INSERT INTO customers(ico, name, town, address) VALUES('123456033', 'Hotel Pod Špilberkem', 'Brno-střed', 'Pekařská 10');
SET @customer_id_32 = LAST_INSERT_ID();
INSERT INTO customers(ico, name, town, address) VALUES('123456034', 'Hotel Pyramida', 'Brno-střed', 'Zahradnická 19');
SET @customer_id_33 = LAST_INSERT_ID();
INSERT INTO customers(ico, name, town, address) VALUES('123456035', 'Hotel Rakovec', 'Brno-Bystrc', 'Rakovecká 13');
SET @customer_id_34 = LAST_INSERT_ID();
INSERT INTO customers(ico, name, town, address) VALUES('123456036', 'Hotel Royal Ricc', 'Brno-střed', 'Starobrněnská 10');
SET @customer_id_35 = LAST_INSERT_ID();
INSERT INTO customers(ico, name, town, address) VALUES('123456037', 'Hotel Santander', 'Brno-Kohoutovice', 'Pisárecká 6');
SET @customer_id_36 = LAST_INSERT_ID();
INSERT INTO customers(ico, name, town, address) VALUES('123456038', 'Hotel Santon', 'Brno-Bystrc', 'Přístavní 38');
SET @customer_id_37 = LAST_INSERT_ID();
INSERT INTO customers(ico, name, town, address) VALUES('123456039', 'Hotel Sharingham', 'Brno-střed', 'Vídeňská 1');
SET @customer_id_38 = LAST_INSERT_ID();
INSERT INTO customers(ico, name, town, address) VALUES('123456040', 'Hotel Slavia', 'Brno-střed', 'Solniční 15/17');
SET @customer_id_39 = LAST_INSERT_ID();
INSERT INTO customers(ico, name, town, address) VALUES('123456041', 'Hotel Slovan', 'Brno-střed', 'Lidická 23');
SET @customer_id_40 = LAST_INSERT_ID();
INSERT INTO customers(ico, name, town, address) VALUES('123456042', 'Hotel Sluneční dvůr', 'Brno-Slatina', 'Přemyslovo náměstí 26');
SET @customer_id_41 = LAST_INSERT_ID();
INSERT INTO customers(ico, name, town, address) VALUES('123456043', 'Hotel Velká Klajdovka', 'Brno-Vinohrady', 'Jedovnická 7');
SET @customer_id_42 = LAST_INSERT_ID();
INSERT INTO customers(ico, name, town, address) VALUES('123456044', 'Hotel Voroněž 1', 'Brno-střed', 'Křížkovského 47');
SET @customer_id_43 = LAST_INSERT_ID();
INSERT INTO customers(ico, name, town, address) VALUES('123456045', 'Hotel Voroněž 2', 'Brno-střed', 'Křížkovského 49');
SET @customer_id_44 = LAST_INSERT_ID();
INSERT INTO customers(ico, name, town, address) VALUES('123456046', 'VV Hotel Garni', 'Brno-střed', 'Mlýnská 8a');
SET @customer_id_45 = LAST_INSERT_ID();
INSERT INTO customers(ico, name, town, address) VALUES('123456047', 'Hotel Žebětínský dvůr', 'Brno-Žebětín', 'Křivánkovo náměstí 33a');
SET @customer_id_46 = LAST_INSERT_ID();

-- Relations

INSERT INTO plan_items(plan, car, customer, day_in_week, week) VALUES(1, @car_id_0, @customer_id_0, 0, NULL);
INSERT INTO plan_items(plan, car, customer, day_in_week, week) VALUES(1, @car_id_1, @customer_id_1, 0, 1);
INSERT INTO plan_items(plan, car, customer, day_in_week, week) VALUES(1, @car_id_1, @customer_id_2, 1, 0);
INSERT INTO plan_items(plan, car, customer, day_in_week, week) VALUES(1, @car_id_0, @customer_id_3, 1, NULL);
INSERT INTO plan_items(plan, car, customer, day_in_week, week) VALUES(1, @car_id_1, @customer_id_4, 1, 1);
INSERT INTO plan_items(plan, car, customer, day_in_week, week) VALUES(1, @car_id_1, @customer_id_5, 2, 0);
INSERT INTO plan_items(plan, car, customer, day_in_week, week) VALUES(1, @car_id_0, @customer_id_6, 2, NULL);
INSERT INTO plan_items(plan, car, customer, day_in_week, week) VALUES(1, @car_id_1, @customer_id_7, 2, 1);
INSERT INTO plan_items(plan, car, customer, day_in_week, week) VALUES(1, @car_id_1, @customer_id_8, 3, 0);
INSERT INTO plan_items(plan, car, customer, day_in_week, week) VALUES(1, @car_id_0, @customer_id_9, 3, NULL);
INSERT INTO plan_items(plan, car, customer, day_in_week, week) VALUES(1, @car_id_1, @customer_id_10, 3, 1);
INSERT INTO plan_items(plan, car, customer, day_in_week, week) VALUES(1, @car_id_1, @customer_id_11, 4, 0);
INSERT INTO plan_items(plan, car, customer, day_in_week, week) VALUES(1, @car_id_0, @customer_id_12, 4, NULL);
INSERT INTO plan_items(plan, car, customer, day_in_week, week) VALUES(1, @car_id_1, @customer_id_13, 4, 1);
INSERT INTO plan_items(plan, car, customer, day_in_week, week) VALUES(1, @car_id_1, @customer_id_14, 0, 0);
INSERT INTO plan_items(plan, car, customer, day_in_week, week) VALUES(1, @car_id_0, @customer_id_15, 0, NULL);
INSERT INTO plan_items(plan, car, customer, day_in_week, week) VALUES(1, @car_id_0, @customer_id_15, 2, NULL);
INSERT INTO plan_items(plan, car, customer, day_in_week, week) VALUES(1, @car_id_1, @customer_id_16, 0, 1);
INSERT INTO plan_items(plan, car, customer, day_in_week, week) VALUES(1, @car_id_1, @customer_id_17, 1, 0);
INSERT INTO plan_items(plan, car, customer, day_in_week, week) VALUES(1, @car_id_0, @customer_id_18, 1, NULL);
INSERT INTO plan_items(plan, car, customer, day_in_week, week) VALUES(1, @car_id_1, @customer_id_19, 1, 1);
INSERT INTO plan_items(plan, car, customer, day_in_week, week) VALUES(1, @car_id_1, @customer_id_20, 2, 0);
INSERT INTO plan_items(plan, car, customer, day_in_week, week) VALUES(1, @car_id_0, @customer_id_21, 2, NULL);
INSERT INTO plan_items(plan, car, customer, day_in_week, week) VALUES(1, @car_id_0, @customer_id_21, 4, NULL);
INSERT INTO plan_items(plan, car, customer, day_in_week, week) VALUES(1, @car_id_1, @customer_id_22, 2, 1);
INSERT INTO plan_items(plan, car, customer, day_in_week, week) VALUES(1, @car_id_1, @customer_id_23, 3, 0);
INSERT INTO plan_items(plan, car, customer, day_in_week, week) VALUES(1, @car_id_0, @customer_id_24, 3, NULL);
INSERT INTO plan_items(plan, car, customer, day_in_week, week) VALUES(1, @car_id_1, @customer_id_25, 3, 1);
INSERT INTO plan_items(plan, car, customer, day_in_week, week) VALUES(1, @car_id_1, @customer_id_26, 4, 0);
INSERT INTO plan_items(plan, car, customer, day_in_week, week) VALUES(1, @car_id_0, @customer_id_27, 4, NULL);
INSERT INTO plan_items(plan, car, customer, day_in_week, week) VALUES(1, @car_id_1, @customer_id_28, 4, 1);
INSERT INTO plan_items(plan, car, customer, day_in_week, week) VALUES(1, @car_id_1, @customer_id_29, 0, 0);
INSERT INTO plan_items(plan, car, customer, day_in_week, week) VALUES(1, @car_id_0, @customer_id_30, 0, NULL);
INSERT INTO plan_items(plan, car, customer, day_in_week, week) VALUES(1, @car_id_1, @customer_id_31, 0, 1);
INSERT INTO plan_items(plan, car, customer, day_in_week, week) VALUES(1, @car_id_1, @customer_id_32, 1, 0);
INSERT INTO plan_items(plan, car, customer, day_in_week, week) VALUES(1, @car_id_0, @customer_id_33, 1, NULL);
INSERT INTO plan_items(plan, car, customer, day_in_week, week) VALUES(1, @car_id_0, @customer_id_33, 2, NULL);
INSERT INTO plan_items(plan, car, customer, day_in_week, week) VALUES(1, @car_id_0, @customer_id_33, 3, NULL);
INSERT INTO plan_items(plan, car, customer, day_in_week, week) VALUES(1, @car_id_1, @customer_id_34, 1, 1);
INSERT INTO plan_items(plan, car, customer, day_in_week, week) VALUES(1, @car_id_1, @customer_id_35, 2, 0);
INSERT INTO plan_items(plan, car, customer, day_in_week, week) VALUES(1, @car_id_0, @customer_id_36, 2, NULL);
INSERT INTO plan_items(plan, car, customer, day_in_week, week) VALUES(1, @car_id_1, @customer_id_37, 2, 1);
INSERT INTO plan_items(plan, car, customer, day_in_week, week) VALUES(1, @car_id_1, @customer_id_38, 3, 0);
INSERT INTO plan_items(plan, car, customer, day_in_week, week) VALUES(1, @car_id_0, @customer_id_39, 3, NULL);
INSERT INTO plan_items(plan, car, customer, day_in_week, week) VALUES(1, @car_id_1, @customer_id_40, 3, 1);
INSERT INTO plan_items(plan, car, customer, day_in_week, week) VALUES(1, @car_id_1, @customer_id_41, 4, 0);
INSERT INTO plan_items(plan, car, customer, day_in_week, week) VALUES(1, @car_id_0, @customer_id_42, 4, NULL);
INSERT INTO plan_items(plan, car, customer, day_in_week, week) VALUES(1, @car_id_1, @customer_id_43, 4, 1);
INSERT INTO plan_items(plan, car, customer, day_in_week, week) VALUES(1, @car_id_1, @customer_id_44, 0, 0);
INSERT INTO plan_items(plan, car, customer, day_in_week, week) VALUES(1, @car_id_0, @customer_id_45, 0, NULL);
INSERT INTO plan_items(plan, car, customer, day_in_week, week) VALUES(1, @car_id_1, @customer_id_46, 0, 1);

-- Testy

CREATE TEMPORARY TABLE IF NOT EXISTS import_errors(`error` VARCHAR(255));
TRUNCATE TABLE import_errors;

INSERT INTO import_errors SELECT 'Wrong cont of cars imported!' FROM cars HAVING COUNT(*) != 2;
INSERT INTO import_errors SELECT 'Wrong cont of customers imported!' FROM customers HAVING COUNT(*) != 47;

INSERT INTO import_errors SELECT 'Wrong cont of plan items imported for customer Hotel A-Austerlitz!' FROM plan_items WHERE `customer` = @customer_id_0 HAVING COUNT(*) != 1;
INSERT INTO import_errors SELECT 'Wrong cont of plan items imported for customer Hotel A-Podlesí!' FROM plan_items WHERE `customer` = @customer_id_1 HAVING COUNT(*) != 1;
INSERT INTO import_errors SELECT 'Wrong cont of plan items imported for customer Hotel Akord!' FROM plan_items WHERE `customer` = @customer_id_2 HAVING COUNT(*) != 1;
INSERT INTO import_errors SELECT 'Wrong cont of plan items imported for customer Hotel Albellus!' FROM plan_items WHERE `customer` = @customer_id_3 HAVING COUNT(*) != 1;
INSERT INTO import_errors SELECT 'Wrong cont of plan items imported for customer Hotel Amphone!' FROM plan_items WHERE `customer` = @customer_id_4 HAVING COUNT(*) != 1;
INSERT INTO import_errors SELECT 'Wrong cont of plan items imported for customer Hotel Arte!' FROM plan_items WHERE `customer` = @customer_id_5 HAVING COUNT(*) != 1;
INSERT INTO import_errors SELECT 'Wrong cont of plan items imported for customer Hotel Avanti!' FROM plan_items WHERE `customer` = @customer_id_6 HAVING COUNT(*) != 1;
INSERT INTO import_errors SELECT 'Wrong cont of plan items imported for customer Hotel Avion!' FROM plan_items WHERE `customer` = @customer_id_7 HAVING COUNT(*) != 1;
INSERT INTO import_errors SELECT 'Wrong cont of plan items imported for customer Hotel Barceló Brno Palace!' FROM plan_items WHERE `customer` = @customer_id_8 HAVING COUNT(*) != 1;
INSERT INTO import_errors SELECT 'Wrong cont of plan items imported for customer Hotel Bílá růže!' FROM plan_items WHERE `customer` = @customer_id_9 HAVING COUNT(*) != 1;
INSERT INTO import_errors SELECT 'Wrong cont of plan items imported for customer Bobycentrum Hotel!' FROM plan_items WHERE `customer` = @customer_id_10 HAVING COUNT(*) != 1;
INSERT INTO import_errors SELECT 'Wrong cont of plan items imported for customer Hotel Brno!' FROM plan_items WHERE `customer` = @customer_id_11 HAVING COUNT(*) != 1;
INSERT INTO import_errors SELECT 'Wrong cont of plan items imported for customer City Apart Hotel!' FROM plan_items WHERE `customer` = @customer_id_12 HAVING COUNT(*) != 1;
INSERT INTO import_errors SELECT 'Wrong cont of plan items imported for customer Hotel Continental!' FROM plan_items WHERE `customer` = @customer_id_13 HAVING COUNT(*) != 1;
INSERT INTO import_errors SELECT 'Wrong cont of plan items imported for customer Hotel Cyro Superior!' FROM plan_items WHERE `customer` = @customer_id_14 HAVING COUNT(*) != 1;
INSERT INTO import_errors SELECT 'Wrong cont of plan items imported for customer Hotel Global!' FROM plan_items WHERE `customer` = @customer_id_15 HAVING COUNT(*) != 2;
INSERT INTO import_errors SELECT 'Wrong cont of plan items imported for customer Grand Hotel Brno!' FROM plan_items WHERE `customer` = @customer_id_16 HAVING COUNT(*) != 1;
INSERT INTO import_errors SELECT 'Wrong cont of plan items imported for customer Hotel Holiday Inn Brno!' FROM plan_items WHERE `customer` = @customer_id_17 HAVING COUNT(*) != 1;
INSERT INTO import_errors SELECT 'Wrong cont of plan items imported for customer Hotel International!' FROM plan_items WHERE `customer` = @customer_id_18 HAVING COUNT(*) != 1;
INSERT INTO import_errors SELECT 'Wrong cont of plan items imported for customer Hotel Jelenice!' FROM plan_items WHERE `customer` = @customer_id_19 HAVING COUNT(*) != 1;
INSERT INTO import_errors SELECT 'Wrong cont of plan items imported for customer Hotel Jonathan!' FROM plan_items WHERE `customer` = @customer_id_20 HAVING COUNT(*) != 1;
INSERT INTO import_errors SELECT 'Wrong cont of plan items imported for customer Hotel Kozák!' FROM plan_items WHERE `customer` = @customer_id_21 HAVING COUNT(*) != 2;
INSERT INTO import_errors SELECT 'Wrong cont of plan items imported for customer Hotel Kozí Horka!' FROM plan_items WHERE `customer` = @customer_id_22 HAVING COUNT(*) != 1;
INSERT INTO import_errors SELECT 'Wrong cont of plan items imported for customer Hotel Maximus Resort!' FROM plan_items WHERE `customer` = @customer_id_23 HAVING COUNT(*) != 1;
INSERT INTO import_errors SELECT 'Wrong cont of plan items imported for customer Hotel Morávka!' FROM plan_items WHERE `customer` = @customer_id_24 HAVING COUNT(*) != 1;
INSERT INTO import_errors SELECT 'Wrong cont of plan items imported for customer Hotel Myslivna!' FROM plan_items WHERE `customer` = @customer_id_25 HAVING COUNT(*) != 1;
INSERT INTO import_errors SELECT 'Wrong cont of plan items imported for customer Hotel Neptun!' FROM plan_items WHERE `customer` = @customer_id_26 HAVING COUNT(*) != 1;
INSERT INTO import_errors SELECT 'Wrong cont of plan items imported for customer Noem Arch Design Hotel!' FROM plan_items WHERE `customer` = @customer_id_27 HAVING COUNT(*) != 1;
INSERT INTO import_errors SELECT 'Wrong cont of plan items imported for customer Hotel Omega!' FROM plan_items WHERE `customer` = @customer_id_28 HAVING COUNT(*) != 1;
INSERT INTO import_errors SELECT 'Wrong cont of plan items imported for customer Hotel Palacký!' FROM plan_items WHERE `customer` = @customer_id_29 HAVING COUNT(*) != 1;
INSERT INTO import_errors SELECT 'Wrong cont of plan items imported for customer Parkhotel Brno!' FROM plan_items WHERE `customer` = @customer_id_30 HAVING COUNT(*) != 1;
INSERT INTO import_errors SELECT 'Wrong cont of plan items imported for customer Hotel Pegas Brno!' FROM plan_items WHERE `customer` = @customer_id_31 HAVING COUNT(*) != 1;
INSERT INTO import_errors SELECT 'Wrong cont of plan items imported for customer Hotel Pod Špilberkem!' FROM plan_items WHERE `customer` = @customer_id_32 HAVING COUNT(*) != 1;
INSERT INTO import_errors SELECT 'Wrong cont of plan items imported for customer Hotel Pyramida!' FROM plan_items WHERE `customer` = @customer_id_33 HAVING COUNT(*) != 3;
INSERT INTO import_errors SELECT 'Wrong cont of plan items imported for customer Hotel Rakovec!' FROM plan_items WHERE `customer` = @customer_id_34 HAVING COUNT(*) != 1;
INSERT INTO import_errors SELECT 'Wrong cont of plan items imported for customer Hotel Royal Ricc!' FROM plan_items WHERE `customer` = @customer_id_35 HAVING COUNT(*) != 1;
INSERT INTO import_errors SELECT 'Wrong cont of plan items imported for customer Hotel Santander!' FROM plan_items WHERE `customer` = @customer_id_36 HAVING COUNT(*) != 1;
INSERT INTO import_errors SELECT 'Wrong cont of plan items imported for customer Hotel Santon!' FROM plan_items WHERE `customer` = @customer_id_37 HAVING COUNT(*) != 1;
INSERT INTO import_errors SELECT 'Wrong cont of plan items imported for customer Hotel Sharingham!' FROM plan_items WHERE `customer` = @customer_id_38 HAVING COUNT(*) != 1;
INSERT INTO import_errors SELECT 'Wrong cont of plan items imported for customer Hotel Slavia!' FROM plan_items WHERE `customer` = @customer_id_39 HAVING COUNT(*) != 1;
INSERT INTO import_errors SELECT 'Wrong cont of plan items imported for customer Hotel Slovan!' FROM plan_items WHERE `customer` = @customer_id_40 HAVING COUNT(*) != 1;
INSERT INTO import_errors SELECT 'Wrong cont of plan items imported for customer Hotel Sluneční dvůr!' FROM plan_items WHERE `customer` = @customer_id_41 HAVING COUNT(*) != 1;
INSERT INTO import_errors SELECT 'Wrong cont of plan items imported for customer Hotel Velká Klajdovka!' FROM plan_items WHERE `customer` = @customer_id_42 HAVING COUNT(*) != 1;
INSERT INTO import_errors SELECT 'Wrong cont of plan items imported for customer Hotel Voroněž 1!' FROM plan_items WHERE `customer` = @customer_id_43 HAVING COUNT(*) != 1;
INSERT INTO import_errors SELECT 'Wrong cont of plan items imported for customer Hotel Voroněž 2!' FROM plan_items WHERE `customer` = @customer_id_44 HAVING COUNT(*) != 1;
INSERT INTO import_errors SELECT 'Wrong cont of plan items imported for customer VV Hotel Garni!' FROM plan_items WHERE `customer` = @customer_id_45 HAVING COUNT(*) != 1;
INSERT INTO import_errors SELECT 'Wrong cont of plan items imported for customer Hotel Žebětínský dvůr!' FROM plan_items WHERE `customer` = @customer_id_46 HAVING COUNT(*) != 1;

INSERT INTO import_errors SELECT 'Table plans doesnt contains default plan (id 1)!' FROM plans WHERE id=1 HAVING COUNT(*) != 1;

-- Vyhodnotenie

SELECT * FROM import_errors;
