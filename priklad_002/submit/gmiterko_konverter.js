XLSX = require('xlsx');

var workbook = XLSX.readFile('DataKPrikladu.xlsx');

var customerSheet = workbook.Sheets[workbook.SheetNames[0]];
var carsSheet = workbook.Sheets[workbook.SheetNames[1]];

/*
 * Customers
 */
var customer = [];
var i = 2;
while (customerSheet["A"+i]) {
	customer.push({
		ico: parseInt(customerSheet["A"+i].v),
		name: customerSheet["B"+i].v,
		town: customerSheet["C"+i].v,
		address: customerSheet["D"+i].v,
		_cars: customerSheet["E"+i].v,
		_week: customerSheet["F"+i].v,
	});
	i++;
}
customer.forEach(function(c){
	c.week = null;
	if(c._week == 'sudý') {
		c.week = 0
	}
	if(c._week == 'lichý') {
		c.week = 1
	}
	delete c._week;
});

/*
 * Cars
 */
var cars = [];
var i = 1;
var col = String.fromCharCode(65 + i);
while (carsSheet[col+"1"]) {
	cars.push(carsSheet[col+"1"].v);
	i++;
	col = String.fromCharCode(65 + i);
}

/*
 * Relations
 */
customer.forEach(function(c){
	relations = [];

	c._cars.split(",").forEach(function(cns){
		var cn = cns.trim();

		//find relation in carsSheet
		var car = null;
		var day_of_week = null;

		var i = 1;
		var col = String.fromCharCode(65 + i);
		while (carsSheet[col+"1"]) {
			var j = 2;
			while (carsSheet[col+j]) {
				if(carsSheet[col+j].v == cn) {
					car = i-1;
					day_of_week = j - 2;
				}
				j++;
			}
			i++;
			col = String.fromCharCode(65 + i);
		}

		if(car != null && day_of_week != null) {
			relations.push({
				car: car,
				day_of_week: day_of_week,
				week: c.week
			});
		}

		c.relations = relations;
		delete c.week;
	});
});

/*
 * Generate SQL
 */

function print(msg) {
	if(!msg) {
		msg = "";
	}
	console.log(msg);
}

print("INSERT IGNORE INTO plans(`id`, `active_from`, `active_to`) VALUES(1, '2016-01-01', '2017-01-01');");

print();
print("-- Cars");
print();

cars.forEach(function(c, i){
	print("INSERT INTO cars(name) VALUES('"+c+"');");
	print("SET @car_id_"+i+" = LAST_INSERT_ID();");
});

print();
print("-- Customers");
print();

customer.forEach(function(c, ci){
	print("INSERT INTO customers(ico, name, town, address) VALUES('"+c.ico+"', '"+c.name+"', '"+c.town+"', '"+c.address+"');");
	print("SET @customer_id_"+ci+" = LAST_INSERT_ID();");
});

print();
print("-- Relations");
print();

customer.forEach(function(c, ci){
	c.relations.forEach(function(r){
		print("INSERT INTO plan_items(plan, car, customer, day_in_week, week) VALUES(1, @car_id_"+r.car+", @customer_id_"+ci+", "+r.day_of_week+", "+(r.week == null ? "NULL" : r.week)+");");
	});
});

print();
print("-- Testy")
print();

print("CREATE TEMPORARY TABLE IF NOT EXISTS import_errors(`error` VARCHAR(255));");
print("TRUNCATE TABLE import_errors;");
print();


//pocet importovanych poloziek
print("INSERT INTO import_errors SELECT 'Wrong cont of cars imported!' FROM cars HAVING COUNT(*) != "+cars.length+";");
print("INSERT INTO import_errors SELECT 'Wrong cont of customers imported!' FROM customers HAVING COUNT(*) != "+customer.length+";");
print();
customer.forEach(function(c, ci){
	print("INSERT INTO import_errors SELECT 'Wrong cont of plan items imported for customer "+c.name+"!' FROM plan_items WHERE `customer` = @customer_id_"+ci+" HAVING COUNT(*) != "+c.relations.length+";");
});
print()

//obsah tabulky palns
print("INSERT INTO import_errors SELECT 'Table plans doesnt contains default plan (id 1)!' FROM plans WHERE id=1 HAVING COUNT(*) != 1;");

print()
print("-- Vyhodnotenie")
print()

print("SELECT * FROM import_errors;")