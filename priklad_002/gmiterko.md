## 2 Vygenerovanie SQL
Kedže štruktúra ktorú som použil v databázr vôbec nezhoduje s formátom dodaných dát napísal som skript ktorý dodaný súbor spracuje. Použil som node.js pretože v ňom bolo najjednoduchšie nainštalovať knižnicu na čítanie xlsx súborov. Program genruje SQL súbor ktorý je možené následne vložiť do databázy. *Použitý skript je taktiež nahraný.*

## 3 Migrácie
Spustil som súbor s dotazmi vygenerovaný skriptom.

## 4 Overenie migrácie
Pretože som už mal pripravený skript generujuci SQL rozhodol som sa doňho pridať aj testy overujúce úspešnú migraciu. Tieto testy porovnávajú dáta uložené v databáze s pôvodnymi záznamami načítanimi z súboru. Vygenerovaný SQL súbor je takisto priložený, testy sa nachádzajú na jeho konci.

- Prvý test overuje či bol importovany správny počet položiek. Kontroluje sa počet vozidiel, zákaznikov a počet naplánovaných rozjazdov pre každého zákaznika.
- Ďalší test kontroluje vloženie základneho plánu. (Plány slúžia pre zmeny pravidelných rozpisov).
- Integritné omedzenia sú skontrolované databázou.